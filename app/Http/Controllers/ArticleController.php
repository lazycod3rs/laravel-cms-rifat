<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Collective\Html\FormFacade;
use Collective\Html\HtmlFacade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Article;
use Illuminate\Support\Facades\Redirect;
use Session;
use View;


class ArticleController extends Controller
{
    //
      /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        // get all the nerds
        $articles = Article::all();

        // load the view and pass the nerds
        return View::make('article.index')->with('article', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        return View::make('article.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
       // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'noofvisit' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('article/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $nerd = new Article;
            $nerd->title       = Input::get('title');
            $nerd->description      = Input::get('description');
            $nerd->noofvisit = Input::get('noofvisit');
            $nerd->save();

            // redirect
            Session::flash('message', 'Successfully created Article!');
            return Redirect::to('article');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $nerd = Article::find($id);

        // show the view and pass the nerd to it
        return View::make('article.show')
            ->with('article', $nerd);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
         // get the nerd
        $nerd = Article::find($id);

        // show the edit form and pass the nerd
        return View::make('article.edit')
            ->with('article', $nerd);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
       // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
            'noofvisit' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('article/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $nerd = Article::find($id);
            $nerd->title       = Input::get('title');
            $nerd->description      = Input::get('description');
            $nerd->noofvisit = Input::get('noofvisit');
            $nerd->save();

            // redirect
            Session::flash('message', 'Successfully updated nerd!');
            return Redirect::to('article');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $nerd = Article::find($id);
        $nerd->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the nerd!');
        return Redirect::to('article');
    }

}
