<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('article') }}">Nerd Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('article') }}">View All Nerds</a></li>
        <li><a href="{{ URL::to('article/create') }}">Create a Nerd</a>
    </ul>
</nav>

<h1>Showing {{ $article->title }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $article->title }}</h2>
        <p>
            <strong>Desc:</strong> {{ $article->description }}<br>
            <strong>No of visits:</strong> {{ $article->noofvisit }}
        </p>
    </div>

</div>
</body>
</html>
